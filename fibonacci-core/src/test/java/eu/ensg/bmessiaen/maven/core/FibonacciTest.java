package eu.ensg.bmessiaen.maven.core;

import org.junit.Assert;
import org.junit.Test;

public class FibonacciTest {

    @Test
    public void calculateTest() {

        Assert.assertEquals(0, Fibonacci.calculate(0));
        Assert.assertEquals(1, Fibonacci.calculate(1));
        Assert.assertEquals(0, Fibonacci.calculate(-1));
        Assert.assertEquals(0, Fibonacci.calculate(Long.MIN_VALUE));
        Assert.assertEquals(233, Fibonacci.calculate(13));
    }
}